Title: plugin_cs_sms
Author: Dr Joseph Baxter <joseph.baxter@nottingham.ac.uk>
Copyright: University of Nottingham 2016 onwards
Description:

plugin_cs_sms was developed for the University of Nottingham.
It allows ExamSys to integration with the CAMPUS SOLUTIONS student management system calling a web services
hosted by a CAMPUS SOLUTIONS server to sync modules, enrolments, users etc.

Installation:

1. Extract plugin_cs_sms archive into the plugins/SMS directory inside ExamSys.
2. Install via the plugins/index.php admin screen.